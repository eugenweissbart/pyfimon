#!/bin/bash
# Prerequisites
sudo apt-get update && sudo apt-get install python python-pip \
python-virtualenv sqlite3 smbclient
# Create virtualenv
virtualenv venv
venv/bin/pip install -r data/requirements.txt
# Create DB
sqlite3 data/pyfimon.sqlite3 << EOF
CREATE TABLE "FILES" ( 
    'ID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    'SERVER' TEXT NOT NULL,
    'SHARE' TEXT NOT NULL,
    'FILE_PATH' TEXT NOT NULL,
    'FILE_NAME' TEXT NOT NULL,
    'FILE_SIZE' INTEGER NOT NULL,
    'FILE_MOD' INTEGER NOT NULL,
    'FILE_HASH' TEXT NOT NULL,
    'UPDATED' INTEGER NOT NULL DEFAULT 1 );
EOF
# Patch smbclient for cyrillic support
cp data/smbclient.patch venv/local/lib/python2.7/site-packages;
(cd venv/local/lib/python2.7/site-packages && patch < smbclient.patch && rm smbclient.patch)
echo "Now it's time to do the following:"
echo 
echo "First of all, copy/move config.yml.sample to config.yml and"
echo "modify it to fulfil your needs"
echo "Then, run the run.sh script manually and check if it works"
echo "Lastly go add the following line to crontab:"
echo
echo "0 12 * * * $(whoami) $(pwd)/run.sh"
echo
echo "************************************************************"
echo "***                    PLEASE DONATE!                    ***"
echo "***                 5244 6873 8752 1049                  ***"
echo "***                   paypal.me/weissy                   ***"
echo "************************************************************"
