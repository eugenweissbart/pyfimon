# -*- coding: utf-8 -*- 
import logging
import logging.handlers
import yaml
from modules.server import Server
from modules.notifier import Notifier

with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

logger    = logging.getLogger('pyfimon')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh        = logging.handlers.RotatingFileHandler(
        filename    = cfg['logging']['logfile'],
        maxBytes    = cfg['logging']['logsize'],
        backupCount = cfg['logging']['logcount'])
fh.setLevel(cfg['logging']['loglevel'])
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.critical("\n\n\n\n\n")

data = []
for server_cfg in cfg['servers']:
    server = Server(server_cfg)
    data.append(server.perform_check())
notifier = Notifier(cfg['notifications'])
notifier.parse_result_and_notify(data)
