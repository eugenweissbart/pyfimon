# -*- coding: utf-8 -*- 
import smbclient
import sqlite3
import os
import sys
import logging
import datetime
import hashlib
import sql

epoch = datetime.datetime.utcfromtimestamp(0)

class Server:
    def __init__(self, server_cfg):
        self.logger     = logging.getLogger('pyfimon')
        self.hostname   = server_cfg['hostname']
        self.domain     = server_cfg['domain']
        self.logger.debug('Server {} initialized'.format(self.hostname))
        self.username   = server_cfg['username']
        self.password   = server_cfg['password']
        self.shares     = server_cfg['shares']
        self.db         = sqlite3.connect("data/pyfimon.sqlite3")
        self.cursor     = self.db.cursor()
        self.logger.debug('DB connection initialized')

    def __del__(self):
        self.cursor.close()
        self.db.commit()
        self.db.close()
        self.logger.debug('DB connection closed')

    def smb_open(self):
        try:
            self.logger.debug('Getting SMB connection')
            self.smb = smbclient.SambaClient(
                server   = self.hostname,
                share    = self.share,
                username = self.username,
                password = self.password,
                domain   = self.domain
            )
            self.smb.listdir('/')
            self.logger.debug('SMB connection acquired')
            return True
        except Exception, e:
            self.logger.error("Error while connecting to share {}:\n\t{}".format(
                self.share,
                str(e)))
            return False

    def smb_close(self):
        self.logger.debug('Closing SMB connection')
        self.smb.close()
        self.logger.debug('SMB connection closed')

    def get_file_info(self, filename):
        data = self.smb.info(os.path.join(self.path, filename))
        date_mod = datetime.datetime.strptime(
            data['write_time'],
            '%a %b %d %I:%M:%S %p %Y %Z'
        )
        fmod = (date_mod - epoch).total_seconds()
        now  = (datetime.datetime.now() - epoch).total_seconds()
        if (now - fmod)/86400 > 2:
            return False
        with self.smb.open(os.path.join(self.path, filename), 'rb') as file:
            file.seek(0, os.SEEK_END)
            fsize = file.tell()
            file.seek(-min(fsize, 10240), os.SEEK_END)
            res   = file.read()
            fhash = hashlib.sha256(res).hexdigest()

        return [fsize, fmod, fhash]

    def check_file(self, filename):
        self.logger.info("  Processing file " + filename)
        data = self.get_file_info(filename)
        if not data:
            return False
        res = self.cursor.execute(
                sql.file_search,
                {
                    "server"  : self.hostname,
                    "share"   : self.share,
                    "path"    : self.path,
                    "fname"   : filename
                }).fetchall()
        if not res:
            self.cursor.execute(
                sql.file_add, {
                "server"    : self.hostname,
                "share"     : self.share,
                "path"      : self.path,
                "fname"     : filename,
                "fsize"     : data[0],
                "fmod"      : data[1],
                "fhash"     : data[2]
                })
            self.db.commit
            return True
        else:
            res = list(res[0])
            if res[0] != data[0]:
                self.logger.info("    File change detected by filesize")
                chg = True
            elif res[1] < data[1]:
                self.logger.info("    File change detected by modify date")
                chg = True
            elif res[2] != data[2]:
                self.logger.info("    File change detected by end hash")
                chg = True
            else:
                self.logger.warn("    File appears to be unchanged!")
                chg = False
            self.cursor.execute(
                    sql.file_update,
                    {
                        "server"    : self.hostname,
                        "share"     : self.share,
                        "path"      : self.path,
                        "fname"     : filename,
                        "fsize"     : data[0],
                        "fmod"      : data[1],
                        "fhash"     : data[2],
                        "chg"       : chg
                    })
            self.db.commit
            return chg

    def process_path(self):
        chg_count = 0
        try:
            self.logger.info("Processing path {}".format(self.path))
            files = self.smb.listdir(self.path)
            for file in files:
                if (self.smb.isfile(os.path.join(self.path , file))):
                    if (self.check_file(file)):
                        chg_count+=1
            res = self.cursor.execute(
                    sql.unchanged_files,
                    {
                        "server"    : self.hostname,
                        "share"     : self.share,
                        "path"      : self.path
                    }).fetchall()
            self.logger.info("Searching for deleted files")
            for file in res:
                if list(file)[1] not in files:
                    self.logger.warn(
                        "{} appears to be deleted from {}".format(
                            list(file)[1],
                            self.path))
                    self.cursor.execute(
                            sql.delete_row,
                            {
                                "id"        : list(file)[0]
                            })
                    self.db.commit()
            return chg_count
        except Exception, e:
            self.logger.warn("Error while processing path {}:\n\t{}".format(
                    self.path,
                    str(e)))
            return -1

    def process_share(self):
        res = []
        self.logger.info("Processing share {}".format(self.share))
        if (self.smb_open()):
            for path in self.paths:
                self.path = path
                chg_count = self.process_path()
                res.append([
                        self.path,
                        chg_count
                    ])
            self.smb_close()
        return res

    def process_shares(self):
        res = []
        for share in self.shares:
            self.share = share['share']
            self.paths = share['paths']
            res.append([
                    self.share,
                    self.process_share()
            ])
        return res

    def perform_check(self):
        chg_count = 0
        self.cursor.execute(
            sql.updated_reset,
            {
                "server"    : self.hostname
            })
        self.db.commit()
        return [self.hostname, self.process_shares()]
