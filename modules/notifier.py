# -*- coding: utf-8 -*-
import logging
import smtplib
import requests
from email.mime.text import MIMEText

class Notifier:
    def __init__(self, config):
        self.logger = logging.getLogger('pyfimon')
        if 'email' not in config:
            self.logger.warn("Email config parameters absent!")
        else:
            self.email_cfg = config['email']
            self.email = self.email_cfg['enabled']
        if 'telegram' not in config:
            self.logger.warn("Telegram config parameters absent!")
        else:
            self.telegram_cfg = config['telegram']
            self.telegram = self.telegram_cfg['enabled']
        self.logger.info("Notifier initialized")
    def email_notify(self, payload):
        if not self.email:
            return -2
        try:
            if self.email_cfg['ssl']:
                s = smtplib.SMTP_SSL(
                    'localhost' if 'server' not in self.email_cfg
                        else self.email_cfg['server'],
                    465 if 'port' not in self.email_cfg
                        else self.email_cfg['port']
                        )
            else:
                s = smtplib.SMTP(
                    'localhost' if 'server' not in self.email_cfg
                        else self.email_cfg['server'],
                    465 if 'port' not in self.email_cfg
                        else self.email_cfg['port']
                        )
            if self.email_cfg['tls']:
                s.starttls()
            text = self.email_cfg['head'].encode('utf8') + '\n' + \
                   payload + '\n' + \
                   self.email_cfg['tail'].encode('utf8')
            msg = MIMEText(text)
            msg['Subject'] = self.email_cfg['subject'].encode('utf8')
            msg['From'] = self.email_cfg['login'].encode('utf8')
            msg['To'] = ', '.join(self.email_cfg['receivers']).encode('utf8')
            s.login(self.email_cfg['login'], self.email_cfg['password'])
            s.sendmail(
                    self.email_cfg['login'],
                    self.email_cfg['receivers'],
                    msg.as_string())
            s.quit()
            return 0
        except Exception, e:
            self.logger.error('Error while trying to send email: {}'.format(str(e)))
            return -1
    def telegram_notify(self, payload):
        if not self.telegram:
            return -2
        try:
            url = "https://api.telegram.org/bot{}/sendMessage"\
                    .format(self.telegram_cfg['api_key'])
            msg = self.telegram_cfg['head'].encode('utf8') + '\n' + payload
            for chat in self.telegram_cfg['chats']:
                data = {"chat_id": chat, "text": msg}
                requests.post(url, data=data).text
            return 0
        except Exception, e:
            self.logger.error('Error while trying to send telegram message: {}'.format(str(e)))
            return -1
    def parse_result_and_notify(self, data):
        report = ''
        srv_no_chg = []
        need_notify = False
        for server in data:
            server_name = server[0]
            shares = server[1]
            for share in shares:
                share_name = share[0]
                paths = share[1]
                if not paths:
                    report += "Невозможно подключиться к " + \
                        "{} на сервере {}\n".format(share_name, server_name)
                for path in paths:
                    path_name = path[0]
                    changed_count = path[1]
                    if changed_count == 0:
                        need_notify = True
                        srv_no_chg.append(server_name)
                        report += "Отсутствуют изменения файлов в директории " + \
                            "{} шары {} на сервере {}\n".format(
                                    path_name,
                                    share_name,
                                    server_name)
                    elif changed_count < 0:
                        report += "Невозможно получить доступ к директории " + \
                            "{} шары {} на сервере {}\n".format(
                                    path_name,
                                    share_name,
                                    server_name)
        if report and need_notify:
            self.email_notify(report)
            report_t = 'Нет изменений на ' + ', '.join(list(set(srv_no_chg)))
            self.telegram_notify(report_t)
