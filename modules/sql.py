updated_reset = """
UPDATE FILES
SET    UPDATED = 0
WHERE  SERVER  = :server
"""
unchanged_files = """
SELECT ID,
       FILE_NAME
FROM   FILES
WHERE  SERVER      = :server
AND    SHARE       = :share
AND    FILE_PATH   = :path
"""

file_search = """
SELECT FILE_SIZE,
       FILE_MOD,
       FILE_HASH
FROM   FILES
WHERE  SERVER      = :server
AND    SHARE       = :share
AND    FILE_PATH   = :path
AND    FILE_NAME   = :fname
"""

file_add = """
INSERT INTO FILES
(
    SERVER,
    SHARE,
    FILE_PATH,
    FILE_NAME,
    FILE_SIZE,
    FILE_MOD,
    FILE_HASH
)
VALUES (
    :server,
    :share,
    :path,
    :fname,
    :fsize,
    :fmod,
    :fhash
);
"""

file_update = """
UPDATE FILES
SET    UPDATED       = :chg,
       FILE_SIZE     = :fsize,
       FILE_MOD      = :fmod,
       FILE_HASH     = :fhash
WHERE  SERVER        = :server
AND    SHARE         = :share
AND    FILE_PATH     = :path
AND    FILE_NAME     = :fname
"""

delete_row = """
DELETE
FROM   FILES
WHERE  ID  = :id
"""
